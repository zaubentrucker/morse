groove_depth = 2;
groove_width = 2.5;
speaker_diameter = 41;
border_width = 7;
holder_thickness = 10;
screw_diameter = 3.2;


difference(){
    //base
    length = speaker_diameter + border_width*2;
    cube([length, holder_thickness, length], center=true);
    
    //cutout
    rotate([90,0,0])
    union(){
        //slot
        cylinder(d=speaker_diameter - groove_depth, h=holder_thickness*2, center=true, $fn=50);
        //groove
        translate([0,0,holder_thickness/2 - groove_width/2 + 0.004])
        cylinder(d=speaker_diameter, h=groove_width, center=true, $fn=50);
    }
    
    //screws
    union(){
        translate([(speaker_diameter/2), 0, (speaker_diameter/2)])
        rotate([90,0,0])
        cylinder(r=screw_diameter/2, h=holder_thickness*2, center=true, $fn=30);
    
        translate([-(speaker_diameter/2), 0, (speaker_diameter/2)])
        rotate([90,0,0])
        cylinder(r=screw_diameter/2, h=holder_thickness*2, center=true, $fn=30);
    
        translate([(speaker_diameter/2), 0, -(speaker_diameter/2)])
        rotate([90,0,0])
        cylinder(r=screw_diameter/2, h=holder_thickness*2, center=true, $fn=30);
    
        translate([-(speaker_diameter/2), 0, -(speaker_diameter/2)])
        rotate([90,0,0])
        cylinder(r=screw_diameter/2, h=holder_thickness*2, center=true, $fn=30);
    }
}