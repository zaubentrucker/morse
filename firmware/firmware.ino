#include <Wire.h>
#include <LiquidCrystal_I2C.h>

#define BUTTON_PIN 4
#define AUDIO_PIN 5
#define CHANGE_DURATION_PIN 7


#define MAXIMUM_LETTER_LENGTH 5
#define LETTER_COUNT (26 + 10)
#define STANDARD_DOT_DURATION_COUNT 5

unsigned long dot_duration = 200ul * 1000ul;
unsigned long dash_duration = dot_duration * 3;
unsigned long letter_spacing = dot_duration * 3;
unsigned long word_spacing = dot_duration * 7;

int letters_written = 0;
bool interrupted = false;

unsigned long standard_dot_durations[STANDARD_DOT_DURATION_COUNT] {
  50000ul,
  100000ul,
  200000ul,
  400000ul,
  800000ul
};

enum letter_state {
  DOT = 0,
  DASH = 1,
  END = 3
};

typedef enum letter_state letter_t[MAXIMUM_LETTER_LENGTH];

const letter_t _letter_a = {
  DOT,DASH,END,END,END
};
const letter_t _letter_b = {
  DASH,DOT,DOT,DOT,END
};
const letter_t _letter_c = {
  DASH,DOT,DASH,DOT,END
};
const letter_t _letter_d = {
  DASH,DOT,DOT,END,END
};
const letter_t _letter_e = {
  DOT,END,END,END,END
};
const letter_t _letter_f = {
  DOT,DOT,DASH,DOT,END
};
const letter_t _letter_g = {
  DASH,DASH,DOT,END,END
};
const letter_t _letter_h = {
  DOT,DOT,DOT,DOT,END
};
const letter_t _letter_i = {
  DOT,DOT,END,END,END
};
const letter_t _letter_j = {
  DOT,DASH,DASH,DASH,END
};
const letter_t _letter_k = {
  DASH,DOT,DASH,END,END
};
const letter_t _letter_l = {
  DOT,DASH,DOT,DOT,END
};
const letter_t _letter_m = {
  DASH,DASH,END,END,END
};
const letter_t _letter_n = {
  DASH,DOT,END,END,END
};
const letter_t _letter_o = {
  DASH,DASH,DASH,END,END
};
const letter_t _letter_p = {
  DOT,DASH,DASH,DOT,END
};
const letter_t _letter_q = {
  DASH,DASH,DOT,DASH,END
};
const letter_t _letter_r = {
  DOT,DASH,DOT,END,END
};
const letter_t _letter_s = {
  DOT,DOT,DOT,END,END
};
const letter_t _letter_t = {
  DASH,END,END,END,END
};
const letter_t _letter_u = {
  DOT,DOT,DASH,END,END
};
const letter_t _letter_v = {
  DOT,DOT,DOT,DASH,END
};
const letter_t _letter_w = {
  DOT,DASH,DASH,END,END
};
const letter_t _letter_x = {
  DASH,DOT,DOT,DASH,END
};
const letter_t _letter_y = {
  DASH,DOT,DASH,DASH,END
};
const letter_t _letter_z = {
  DASH,DASH,DOT,DOT,END
};
const letter_t _letter_0 = {
  DASH,DASH,DASH,DASH,DASH
};
const letter_t _letter_1 = {
  DOT,DASH,DASH,DASH,DASH
};
const letter_t _letter_2 = {
  DOT,DOT,DASH,DASH,DASH
};
const letter_t _letter_3 = {
  DOT,DOT,DOT,DASH,DASH
};
const letter_t _letter_4 = {
  DOT,DOT,DOT,DOT,DASH
};
const letter_t _letter_5 = {
  DOT,DOT,DOT,DOT,DOT
};
const letter_t _letter_6 = {
  DASH,DOT,DOT,DOT,DOT
};
const letter_t _letter_7 = {
  DASH,DASH,DOT,DOT,DOT
};
const letter_t _letter_8 = {
  DASH,DASH,DASH,DOT,DOT
};
const letter_t _letter_9 = {
  DASH,DASH,DASH,DASH,DOT
};

const letter_t* letter_index[LETTER_COUNT]{
  &_letter_a,
  &_letter_b,
  &_letter_c,
  &_letter_d,
  &_letter_e,
  &_letter_f,
  &_letter_g,
  &_letter_h,
  &_letter_i,
  &_letter_j,
  &_letter_k,
  &_letter_l,
  &_letter_m,
  &_letter_n,
  &_letter_o,
  &_letter_p,
  &_letter_q,
  &_letter_r,
  &_letter_s,
  &_letter_t,
  &_letter_u,
  &_letter_v,
  &_letter_w,
  &_letter_x,
  &_letter_y,
  &_letter_z,
  &_letter_0,
  &_letter_1,
  &_letter_2,
  &_letter_3,
  &_letter_4,
  &_letter_5,
  &_letter_6,
  &_letter_7,
  &_letter_8,
  &_letter_9
};


LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE); 


bool debouncedDigitalRead(int pin){
  int duration = 10;
  int test_count = 10;
  
  int res_positive = 0;
  int res_negative = 0;
  int time_left = duration;
  for(int i = 0; i<test_count; i++){
    if(digitalRead(pin) == HIGH){
      res_positive++;
    }
    else{
      res_negative++;
    }
    delay(duration/test_count);
  }
  if(res_positive >= res_negative){
    return true;
  }
  return false;
}


bool lettersEqual(letter_t* object, letter_t* other){
  for(int i = 0; i < MAXIMUM_LETTER_LENGTH; i++){
    if((*object)[i] != (*other)[i])
      return false;
  }
  return true;
}

char decodeLetter(letter_t* l){
  for(int i = 0; i < LETTER_COUNT; i++){
    if(lettersEqual(l, letter_index[i])){
      if(i >= 26){  // letter is a number
        return (char) (48 + i - 26);
      }
      else{ // letter is in the alphabet
        return (char) (65 + i);
      }
    }
  }
  return '?';
}


void setup() 
{
  
  //set DDRS
  pinMode(BUTTON_PIN, INPUT);
  pinMode(CHANGE_DURATION_PIN, INPUT);
  pinMode(AUDIO_PIN, OUTPUT);

  //set pullups
  digitalWrite(BUTTON_PIN, HIGH);
  digitalWrite(CHANGE_DURATION_PIN, HIGH);

  attachInterrupt(digitalPinToInterrupt(CHANGE_DURATION_PIN), changeDotDuration, FALLING);

  //init lcd
  lcd.begin(16,2);
  lcd.backlight();
  lcd.setCursor(0,0);

  
  //wait for debouncing capacitor to load
  delay(100);

  

}

void changeDotDuration(){
  interrupted = true;
  noInterrupts();

}

bool readLetter(letter_t* letter){
  for(int i = 0; i < MAXIMUM_LETTER_LENGTH; i++){
    unsigned long hold_micros = micros();
    tone(AUDIO_PIN,300);
    while(!debouncedDigitalRead(BUTTON_PIN));
    noTone(AUDIO_PIN);
    hold_micros = micros() - hold_micros;
    unsigned long release_micros = micros();
    while(debouncedDigitalRead(BUTTON_PIN) && micros() - release_micros < word_spacing);
    release_micros = micros() - release_micros;

    if(hold_micros < (dot_duration + dash_duration) / 2){
      (*letter)[i] = DOT;
    }
    else{
      (*letter)[i] = DASH; 
    }

    if(release_micros > (word_spacing + letter_spacing) / 2){
      return true;
    }
    if(release_micros > (letter_spacing + dot_duration) / 2){
      return false;
    }
  }
  return false;
}

void writeLetter(char letter){
  if(letters_written == 16){
    lcd.setCursor(0,1);
  }
  if(letters_written == 32){
    lcd.clear();
    lcd.setCursor(0,0);
    letters_written = 0;
  }
  letters_written += 1;
  lcd.print(letter);
}



void loop() 
{ 

  
  bool word_ends = false;
  while(debouncedDigitalRead(BUTTON_PIN)){
    if(interrupted){
      //change dot duration
      static int dot_duration_ptr = 2;
      dot_duration_ptr = (dot_duration_ptr + 1) % STANDARD_DOT_DURATION_COUNT;
      dot_duration = standard_dot_durations[dot_duration_ptr];
      dash_duration = dot_duration * 3;
      letter_spacing = dot_duration * 3;
      word_spacing = dot_duration * 7;

      //display changes
      lcd.clear();
      lcd.setCursor(0,0);
      lcd.print("Dot-duratn. now:");
      lcd.setCursor(0,1);
      lcd.print(dot_duration/1000);
      lcd.print("ms");
      delay(1000);
      lcd.clear();
      letters_written = 0;
      interrupts();
      interrupted = false;
    }
  }
  while(!word_ends){
    letter_t letter = {END,END,END,END,END};
    word_ends = readLetter(&letter);
    writeLetter(decodeLetter(&letter));
    if(!word_ends)
      while(debouncedDigitalRead(BUTTON_PIN));
  }
  writeLetter(' ');

  
}
