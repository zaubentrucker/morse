# Morse Decoder

A switch that is connected to an arduino pro-micro is used to encode morse. The encoded letters/numbers are then written on an LCD screen.

![you will not regret it](https://git.rwth-aachen.de/zaubentrucker/morse/-/raw/master/please_hire_me.jpeg)
